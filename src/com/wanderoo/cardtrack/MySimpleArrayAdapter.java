package com.wanderoo.cardtrack;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MySimpleArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	protected static ManageCardsActivity manageCardsActivity = null;

	public MySimpleArrayAdapter(Context context, ArrayList<String> resources) {
		super(context, R.layout.card_info, resources);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.card_info, parent, false);
		TextView cardName = (TextView) rowView.findViewById(R.id.cardName);
		
		cardName.setText(CardTrackActivity.cards.get(position).get(1)+" ( "+CardTrackActivity.cards.get(position).get(3) + " )");
		
		Typeface face = Typeface.createFromAsset(manageCardsActivity.getAssets(), "fonts/COMIC.TTF");
		cardName.setTypeface(face, Typeface.BOLD);
		
		return rowView;
	}
}