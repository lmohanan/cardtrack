package com.wanderoo.cardtrack;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ViewEditCategoryActivity extends Activity
{
	protected static int rowSelected;
	EditText CategoryName = null;
	protected static ManageCategoryActivity manageCategorysActivity;
	protected static String OldCatName = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.view_edit_category_layout);
		
		Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ALBA.TTF");
		TextView viewCatTitle = (TextView)findViewById(R.id.categoryDetails);
		viewCatTitle.setTypeface(face);
		
		CategoryName = (EditText) findViewById(R.id.categoryName);
		
		face = Typeface.createFromAsset(getAssets(), "fonts/Harabara.ttf");
		CategoryName.setTypeface(face);
		CategoryName.setText(CardTrackActivity.categories.get(rowSelected).get(1));
		OldCatName = CategoryName.getText().toString();
	}
	
	public void editCategory(View view)
	{
		if(this.CategoryName.getText().toString().equals("")){
			Toast.makeText(ViewEditCategoryActivity.this, "Please enter category name...", Toast.LENGTH_LONG).show();
			return;
		}
		showDialog(11);
	}
	public void editTheCategory(){
		String CategoryName = this.CategoryName.getText().toString();
		//int CategoryID = this.c
		CardTrackActivity.dataSource.open();
		
		if(CardTrackActivity.dataSource.updateCategoryDetails(OldCatName, CategoryName))
		{
			CardTrackActivity.categories.get(rowSelected).set(1, CategoryName.toString());
			Toast.makeText(ViewEditCategoryActivity.this, "Category name updated successfully...", Toast.LENGTH_LONG).show();
			manageCategorysActivity.loadCategories();
			
			for(int i=0;i<CardTrackActivity.payments.size();i++){
				if(CardTrackActivity.payments.get(i).get(5).equals(OldCatName))
					CardTrackActivity.payments.get(i).set(5, CategoryName);
			}
		} 
		else 
		{
			Toast.makeText(ViewEditCategoryActivity.this, "Error! Category not updated...", Toast.LENGTH_LONG).show();
		}
		CardTrackActivity.dataSource.close();
		ViewEditCategoryActivity.this.finish();
	}
	
	public void deleteCategory(View view)
	{
//		String CategoryName = this.CategoryName.getText().toString();
//		
//		CardTrackActivity.dataSource.open();
//		if(CardTrackActivity.dataSource.deleteCategory(CategoryName))
//		{
//			Toast.makeText(ViewEditCategoryActivity.this, "Category deleted successfully...", Toast.LENGTH_LONG).show();
//			CardTrackActivity.categories.clear();
//			CardTrackActivity.loadCategory();
//			CardTrackActivity.dataSource.close();
//			ManageCategoryActivity.deleteCategory(rowSelected);
//			this.finish();
//		} 
//		else 
//		{
//			Toast.makeText(ViewEditCategoryActivity.this, "Error! Category not updated...", Toast.LENGTH_LONG).show();
//		}
//		CardTrackActivity.dataSource.close();
	}
	
	public void cancel(View view)
	{
		this.finish();
	}
	
	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();
		this.finish();
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog dialog = null;
		Builder builder1 = null;
		switch (id) {
		case 11:
			// Create out AlterDialog
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("Are you sure you want to update the category?");
			builder1.setCancelable(true);
			builder1.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Toast.makeText(getApplicationContext(),
									"Category updation cancelled...", Toast.LENGTH_SHORT).show();
						}
					});
			builder1.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							editTheCategory();
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		/*case 12:
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("Are you sure you want to update the card?");
			builder1.setCancelable(true);
			builder1.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Toast.makeText(getApplicationContext(),
									"Card updation cancelled...", Toast.LENGTH_SHORT).show();
						}
					});
			builder1.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							editTheCard();
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		*/}
		return dialog;
	}
}
