package com.wanderoo.cardtrack;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CatSummaryArrayAdaptor extends ArrayAdapter<String> 
{
	private final Context context;
	protected static CatSummaryActivity catSummaryActivity = null;
	
	public CatSummaryArrayAdaptor(Context context, ArrayList<String> resources) 
	{
		super(context, R.layout.category_total, resources);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.category_total, parent, false);
		TextView catName = (TextView) rowView.findViewById(R.id.catName);
		TextView catTotal = (TextView) rowView.findViewById(R.id.catTotal);
		
		catName.setText(CatSummaryActivity.categoryTotals.get(position).get(0));
		catTotal.setText(CatSummaryActivity.categoryTotals.get(position).get(1));
		
		Typeface face = Typeface.createFromAsset(catSummaryActivity.getAssets(), "fonts/simplicity.ttf");
		catName.setTypeface(face, Typeface.BOLD);
		catTotal.setTypeface(face, Typeface.BOLD);
		
		return rowView;
	}
}