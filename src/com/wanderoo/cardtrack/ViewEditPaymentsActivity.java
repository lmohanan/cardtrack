package com.wanderoo.cardtrack;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ViewEditPaymentsActivity extends FragmentActivity
{
	protected boolean continueYes = true;
	protected static int rowSelected;
	protected EditText PaymentDesc = null;
	protected EditText PaymentAmt = null;
	private Double oldPaymentAmt = null;
	protected Spinner paymentCategory = null;
	protected Spinner paymentCard = null;
	protected Button paymentDate = null;
	protected String payDate = null;
	protected static ViewEditPaymentsActivity thisActivity = null;
	private int year;
	private String day;
	private String month;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		thisActivity = this;
		PaymentDatePickerFragment.editing=true;
		setContentView(R.layout.view_edit_payment_layout);
		
		Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ALBA.TTF");
		TextView viewPaymentTitle = (TextView)findViewById(R.id.viewPayment);
		viewPaymentTitle.setTypeface(face);
		
		PaymentDesc = (EditText) findViewById(R.id.PaymentDesc);
		PaymentAmt = (EditText) findViewById(R.id.PaymentAmt);
		
		face = Typeface.createFromAsset(getAssets(), "fonts/Harabara.ttf");
		PaymentDesc.setTypeface(face);
		PaymentAmt.setTypeface(face);
		
		PaymentDesc.setText(CardTrackActivity.payments.get(rowSelected).get(2));
		PaymentAmt.setText(CardTrackActivity.payments.get(rowSelected).get(3));
		oldPaymentAmt = Double.valueOf(CardTrackActivity.payments.get(rowSelected).get(3));
		
		paymentCard = (Spinner) findViewById(R.id.PaymentCard);

		List<String> list = new ArrayList<String>();
		if (CardTrackActivity.cards.size() > 0)
			for (int i = 0; i < CardTrackActivity.cards.size(); i++)
				list.add(CardTrackActivity.cards.get(i).get(1).toString()+":"+CardTrackActivity.cards.get(i).get(3).toString());

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		paymentCard.setAdapter(dataAdapter);
		for(int i=0;i<CardTrackActivity.cards.size();i++){
			if((CardTrackActivity.cards.get(i).get(1)+":"+CardTrackActivity.cards.get(i).get(3)).equals(CardTrackActivity.payments.get(rowSelected).get(4))){
				paymentCard.setSelection(i, true);
				break;
			}
		}

		paymentCategory = (Spinner) findViewById(R.id.PaymentCategory);
		list = new ArrayList<String>();
		if (CardTrackActivity.categories.size() > 0)
			for (int i = 0; i < CardTrackActivity.categories.size(); i++)
				list.add(CardTrackActivity.categories.get(i).get(1));

		dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		paymentCategory.setAdapter(dataAdapter);
		
		for(int i=0;i<CardTrackActivity.categories.size();i++){
			if(CardTrackActivity.categories.get(i).get(1).equals(CardTrackActivity.payments.get(rowSelected).get(5))){
				paymentCategory.setSelection(i, true);
				break;
			}
		}
		
		paymentDate = (Button) findViewById(R.id.paymentDate);
		paymentDate.setText("Payment Date\n"+CardTrackActivity.payments.get(rowSelected).get(1));
		payDate = CardTrackActivity.payments.get(rowSelected).get(1);
		paymentDate.setTypeface(face);
	}
	
	public void editPayment(View view)
	{
		// Vaidate and set defaults for amt
		if (this.PaymentAmt.getText().length() <= 0) {
			Toast.makeText(this, "Please enter an amount...", Toast.LENGTH_LONG)
					.show();
			return;
		} else {
			try {
				Double.parseDouble(this.PaymentAmt.getText().toString());
			} catch (NumberFormatException ne) {
				Toast.makeText(this,"Please enter an amount greater than zero...",Toast.LENGTH_LONG).show();
				return;
			}
		}
		
		String paymentDescription = null;
		if (this.PaymentDesc.getText().toString().equals("")
				|| this.PaymentDesc.getText().toString().equals(
						"Enter Payment Description"))
			paymentDescription = paymentCategory + " (" + payDate + ")";
		else
			paymentDescription = this.PaymentDesc.getText().toString();
		
		CardTrackActivity.dataSource.open();
		Double balance = Double.valueOf(CardTrackActivity.cardIdMap.get(paymentCard.getSelectedItem().toString()))-(Double.valueOf(PaymentAmt.getText().toString())-oldPaymentAmt);
		
		if(CardTrackActivity.dataSource.updatePaymentDetails(Integer.valueOf(CardTrackActivity.payments.get(rowSelected).get(0)), payDate,
				paymentDescription, PaymentAmt.getText().toString(), paymentCard.getSelectedItem().toString(),
				paymentCategory.getSelectedItem().toString(), balance.toString()))
		{
			CardTrackActivity.cardIdMap.put(paymentCard.getSelectedItem().toString(), balance.toString());

			CardTrackActivity.payments.get(rowSelected).set(1, payDate);
			CardTrackActivity.payments.get(rowSelected).set(2, paymentDescription);
			CardTrackActivity.payments.get(rowSelected).set(3, PaymentAmt.getText().toString());
			CardTrackActivity.payments.get(rowSelected).set(4, paymentCard.getSelectedItem().toString());
			CardTrackActivity.payments.get(rowSelected).set(5, paymentCategory.getSelectedItem().toString());
			
			Toast.makeText(ViewEditPaymentsActivity.this, "Payment updated successfully...", Toast.LENGTH_LONG).show();
			
			ViewPaymentsActivity.thisActivity.loadPayments();
		} 
		else 
		{
			Toast.makeText(ViewEditPaymentsActivity.this, "Error! Category not updated...", Toast.LENGTH_LONG).show();
		}
		CardTrackActivity.dataSource.close();
		ViewEditPaymentsActivity.this.finish();
	}
	
	public void cancel(View view)
	{
		this.finish();
	}
	
	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();
		this.finish();
	}
	
	public void showDatePickerDialog(View view) {
		DialogFragment newFragment = new PaymentDatePickerFragment();
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}
	
	public void continueWithDateSelection(int year, String month2, String dayOfTheMonth){
		this.year = year;
		this.month = month2;
		this.day = dayOfTheMonth;
		showDialog(16);
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog dialog = null;
		Builder builder1 = null;
		switch (id) {
		case 16:
			// Create out AlterDialog
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("ALERT: The date you selected is in the future. Are you sure you want to continue?");
			builder1.setCancelable(true);
			builder1.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			builder1.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							ViewEditPaymentsActivity.thisActivity.paymentDate.setText("Payment Date\n"
									+ year + "/"
									+ month + "/" + day);
							ViewEditPaymentsActivity.thisActivity.payDate = + year + "/"
									+ month + "/" + day;
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		
		}
		return dialog;
	}

}
