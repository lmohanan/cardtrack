package com.wanderoo.cardtrack;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class CatSummaryActivity extends Activity 
{
	CatSummaryArrayAdaptor listOfCatTotal = null;
	ArrayList<String> resources = new ArrayList<String>();
	Spinner selectMonth = null;
	Spinner selectYear = null;
	TextView noRecords = null;
	TextView categoryName = null;
	TextView categoryTotal = null;
		
	public static SparseArray<ArrayList<String>> categoryTotals = new SparseArray<ArrayList<String>>();
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.category_summary);
		CatSummaryArrayAdaptor.catSummaryActivity=this;
		noRecords = (TextView) findViewById(R.id.noRecords);
		
		categoryName = (TextView) findViewById(R.id.categoryName);
		categoryTotal = (TextView) findViewById(R.id.categoryTotal);
		
		categoryName.setText("Category Name");
		categoryTotal.setText("Total Expenses");
		
		Typeface face = Typeface.createFromAsset(getAssets(), "fonts/simplicity.ttf");
		categoryName.setTypeface(face, Typeface.BOLD);
		categoryTotal.setTypeface(face, Typeface.BOLD);
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int mon = Calendar.getInstance().get(Calendar.MONTH);
		CardTrackActivity.dataSource.open();
		
		categoryTotals = CardTrackActivity.dataSource.getCatSummary(mon+1, year);
		for(int i=0; i<categoryTotals.size();i++)
		{
			resources.add(Integer.toString(i));
		}
		
		listOfCatTotal = new CatSummaryArrayAdaptor(this, resources);
		ListView categoryListView = (ListView) findViewById(R.id.catList);
		categoryListView.setAdapter(listOfCatTotal);
		
		selectMonth = (Spinner) findViewById(R.id.selectMonth);
		selectYear = (Spinner) findViewById(R.id.selectYear);

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, CardTrackActivity.monthList);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		selectMonth.setAdapter(dataAdapter);
		
		dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, CardTrackActivity.yearList);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		selectYear.setAdapter(dataAdapter);
		
		selectMonth.setSelection(mon);
		selectYear.setSelection(year-2000);
	}
	
	public void onResume() 
	{
		super.onResume();
	}

	public void onBackPressed() 
	{
		super.onBackPressed();
		CatSummaryActivity.this.finish();
	};

	public void getRecords(View view) 
	{
		categoryTotals = CardTrackActivity.dataSource.getCatSummary(
				selectMonth
						.getSelectedItemPosition()+1,
				Integer.valueOf(selectYear.getSelectedItem().toString())
						.intValue());
		resources.clear();
		for(int i=0; i<categoryTotals.size();i++)
		{
			resources.add(Integer.toString(i));
		}
		listOfCatTotal.notifyDataSetChanged();
		if(categoryTotals.size()<1){
			noRecords.setVisibility(View.VISIBLE);
			noRecords.setText("No Records Found!");
		} else {
			noRecords.setVisibility(View.GONE);
		}
	}

}