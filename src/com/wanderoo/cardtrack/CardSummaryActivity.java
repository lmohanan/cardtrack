package com.wanderoo.cardtrack;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class CardSummaryActivity extends Activity 
{
	CardSummaryArrayAdaptor listOfCardTotal = null;
	ArrayList<String> resources = new ArrayList<String>();
	protected static SparseArray<ArrayList<String>> cardTotals = new SparseArray<ArrayList<String>>();
	Spinner selectMonth = null;
	Spinner selectYear = null;
	TextView cardName = null;
	TextView cardTotal = null;
	TextView cardBalance = null;
	protected static DecimalFormat df = new DecimalFormat("#.##");
	
	protected static CardSummaryActivity thisActivity = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.card_summary);
		CardSummaryArrayAdaptor.cardSummaryActivity=this;
		thisActivity = this;
		
		cardName = (TextView) findViewById(R.id.cardName);
		cardTotal = (TextView) findViewById(R.id.cardTotal);
		cardBalance = (TextView) findViewById(R.id.cardBalance);
		
		cardName.setText("Card Name");
		cardTotal.setText("Total Expenses");
		cardBalance.setText("Balance");
		
		Typeface face = Typeface.createFromAsset(getAssets(), "fonts/simplicity.ttf");
		cardName.setTypeface(face, Typeface.BOLD);
		cardTotal.setTypeface(face, Typeface.BOLD);
		face = Typeface.createFromAsset(getAssets(), "fonts/Harabara.ttf");
		cardBalance.setTypeface(face, Typeface.BOLD);

		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int mon = Calendar.getInstance().get(Calendar.MONTH);
		int date = Calendar.getInstance().get(Calendar.DATE);
		CardTrackActivity.dataSource.open();
		
		cardTotals = CardTrackActivity.dataSource.getCardSummary(year, mon,date);
		for(int i=0; i<cardTotals.size();i++)
		{
			resources.add(Integer.toString(i));
		}
		
		listOfCardTotal = new CardSummaryArrayAdaptor(this, resources);
		ListView cardList = (ListView) findViewById(R.id.cardList);
		cardList.setAdapter(listOfCardTotal);
		cardList.setOnItemClickListener(displaySettlementInfo);
		
		selectMonth = (Spinner) findViewById(R.id.selectMonth);
		selectYear = (Spinner) findViewById(R.id.selectYear);
		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, CardTrackActivity.monthList);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		selectMonth.setAdapter(dataAdapter);
		
		dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, CardTrackActivity.yearList);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		selectYear.setAdapter(dataAdapter);
		
		selectMonth.setSelection(mon);
		selectYear.setSelection(year-2000);
	}
	
	public void onResume() 
	{
		super.onResume();
	}

	private OnItemClickListener displaySettlementInfo = new OnItemClickListener() 
	{
		public void onItemClick(AdapterView<?> av, View v, int pos, long id) 
		{
			CardSettlementActivity.rowSelected=pos;
			try{
				startActivity(new Intent(CardSummaryActivity.thisActivity, CardSettlementActivity.class));
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	public void onBackPressed() 
	{
		super.onBackPressed();
		CardSummaryActivity.this.finish();
	};

	public void getRecords(View view) 
	{
		CardTrackActivity.dataSource.open();
		cardTotals = CardTrackActivity.dataSource.getCardSummary(Integer
				.valueOf(selectYear.getSelectedItem().toString()).intValue(),
				selectMonth.getSelectedItemPosition(), 1);
		
		resources.clear();
		for(int i=0; i<cardTotals.size();i++)
		{
			resources.add(Integer.toString(i));
		}
		
		listOfCardTotal.notifyDataSetChanged();
		CardTrackActivity.dataSource.close();
	}
	
	public void loadCards() 
	{
		if(null!=listOfCardTotal)
			listOfCardTotal.notifyDataSetChanged();
	}

}