package com.wanderoo.cardtrack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.SparseArray;

public class CardTrackDataSource {
	private SQLiteDatabase database;
	private DataManager manager;
	protected static String[] cardColumns = {"id","card_name","card_limit","card_type", "billing_date", "balance"};
	protected static String[] CategoryColumns = {"cid","category_name"};
	protected static String[] paymentColumns = {"pid","date", "desc", "amt", "card_id","cat_id"};
	private String[] CardSummaryColumns = {"card_id","SUM(amt)"};
	private String[] BillingDateColumns = {"id","card_name","card_type","billing_date"};
	protected static String[] CatSummaryColumns = {"cat_id","SUM(amt)"};
	
	
	public CardTrackDataSource(Context context){
		manager = new DataManager(context);
	}
	
	public void open() throws SQLException {
		database = manager.getWritableDatabase();
	}
	
	public void close() {
		database.close();
		manager.close();
	}
	
	protected boolean insertCardDetails(String card_name, String card_limit, String card_type, String billingDate, String balance){
		ContentValues values = new ContentValues();
		values.put("card_name", card_name);
		values.put("card_limit", card_limit);
		values.put("card_type", card_type);
		values.put("billing_date", billingDate);
		values.put("balance", card_limit);
		
		if(database.insert("card_details", null, values)!=-1)
			return true;
		return false;
	}
	
	protected void getCards(/*Map<Integer, ArrayList<String>>*/SparseArray<ArrayList<String>> cardNames){
		Cursor cursor = database.query("card_details", cardColumns, null, null, null, null, null);
		cursor.moveToFirst();
		ArrayList<String> values = null;
		for(int i=0; i<cursor.getCount();i++){
			values = new ArrayList<String>();
			values.add(cursor.getString(0));
			values.add(cursor.getString(1));
			values.add(cursor.getString(2));
			values.add(cursor.getString(3));
			values.add(cursor.getString(4));
			values.add(cursor.getString(5));
			cardNames.put(i, values);
			cursor.moveToNext();
		}
		cursor.close();
	}
	
	protected String[] getCardDescription(String card_name){
		Cursor cursor = database.query("card_details", cardColumns,"card_name = '"+card_name+"'", null, null, null, null);
		cursor.moveToFirst();
		String[] details = {cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4)};
		cursor.close();
		return details;
	}

	public boolean checkIfCardExists(String cardName, String cardType) {
		String[] id = {"id"};
		Cursor cursor = database.query("card_details", id, "card_name = '"+cardName+"' AND card_type = '"+cardType+"'", null, null, null, null);
		if(cursor.getCount()>0){
			return true;
		}
		return false;
	}

	public boolean updateCardDetails(String oldCardName, Integer id, String cardName, String cardLimit,
			String cardType, String billingDate, String balance) {
		ContentValues values = new ContentValues();
		values.put("card_id",cardName);
		
		String[] whereArgs = {id.toString()};
		
		if(-1 != database.update("payment_details", values, "card_id = '"+oldCardName+"'", null)){
			
			values.clear();
			values.put("card_name",cardName);
			values.put("card_type",cardType);
			values.put("billing_date",billingDate);
			values.put("card_limit", cardLimit);
			values.put("balance", balance);
			
			if(-1!=database.update("card_details", values, "id = ?", whereArgs))
				return true;
		}
		return false;
	}
	
//***************************************Category Functions******************************************************	
	
	protected boolean insertCategoryDetails(String category_name)
	{
		ContentValues values = new ContentValues();
		values.put("category_name", category_name);
		
		if(database.insert("category_details", null, values)!=-1)
			return true;
		return false;
	}

	public boolean deleteCard(String cardName, String cardType) {
		if(-1 != database.delete("payment_details", "card_id = '"+cardName+"'", null)){
			if(-1 != database.delete("card_details", "card_name = '"+cardName+"' AND card_type = '"+cardType+"'", null)){
				return true;
			}
		}
		return false;
	}
	
	public boolean updateCategoryDetails(String oldCatName, String categoryName) 
	{
		ContentValues values = new ContentValues();
		values.put("cat_id", categoryName);
		
		if(-1 != database.update("payment_details", values, "cat_id = '"+oldCatName+"'", null)){
			values.clear();
			values.put("category_name", categoryName);
			if(-1!=database.update("category_details", values, "category_name = '"+oldCatName+"'", null))
				return true;
		}
		return false;
	}

	public boolean checkIfCategoryExists(String categoryName) 
	{
		String[] cid = {"cid"};
		Cursor cursor = database.query("category_details", cid, "category_name = '"+categoryName+"'", null, null, null, null);
		if(cursor.getCount()>0)
		{
			return true;
		}
		return false;
	}
	
	protected String[] getCategoriesDescription(String category_name)
	{
		Cursor cursor = database.query("category_details", CategoryColumns,"category_name = '"+category_name+"'", null, null, null, null);
		cursor.moveToFirst();
		String[] details = {cursor.getString(0), cursor.getString(1)};
		cursor.close();
		return details;
	}

	protected void getCategories(SparseArray<ArrayList<String>> categoryNames)
	{
		Cursor cursor = database.query("category_details", CategoryColumns, null, null, null, null, null);
		cursor.moveToFirst();
		ArrayList<String> values = null;
		for(int i=0; i<cursor.getCount();i++)
		{
			values = new ArrayList<String>();
			values.add(cursor.getString(0));
			values.add(cursor.getString(1));
			categoryNames.put(i, values);
			cursor.moveToNext();
		}
		cursor.close();
	}

	
//***************************************Payment Functions******************************************************	
	
	protected boolean insertPaymentDetails(String pmt_date, String pmt_desc, String pmt_amt, String pmt_card_id,
			String pmt_category_id, String cardBalance)
	{
		ContentValues values = new ContentValues();
		values.put("balance", cardBalance);
		
		if(-1 != database.update("card_details", values, "card_name = '"+pmt_card_id.split(":")[0]+
				"' AND card_type='"+pmt_card_id.split(":")[1]+"'", null)){
			values.clear();
			values.put("date", pmt_date);
			values.put("desc", pmt_desc);
			values.put("amt", pmt_amt);
			values.put("card_id", pmt_card_id);
			values.put("cat_id", pmt_category_id);
			
			if(database.insert("payment_details", null, values)!=-1)
				return true;
		}
		return false;
	}

	public void getPayments(SparseArray<ArrayList<String>> payments) {
		Cursor cursor = database.query("payment_details", paymentColumns, null, null, null, null, null);
		cursor.moveToFirst();
		ArrayList<String> values = null;
		for(int i=0; i<cursor.getCount();i++)
		{
			values = new ArrayList<String>();
			values.add(cursor.getString(0));
			values.add(cursor.getString(1));
			values.add(cursor.getString(2));
			values.add(cursor.getString(3));
			values.add(cursor.getString(4));
			values.add(cursor.getString(5));
			payments.put(i, values);
			cursor.moveToNext();
		}
		cursor.close();
	}

	public boolean updatePaymentDetails(Integer pid, String payDate, String desc, String amt, String card, String category, String balance) {
		ContentValues values = new ContentValues();
		values.put("balance", balance);
		
		if(-1 != database.update("card_details", values, "card_name = '"+card.split(":")[0]+
				"' AND card_type='"+card.split(":")[1]+"'", null)){
			values.clear();
			values.put("date", payDate);
			values.put("desc", desc);
			values.put("amt", amt);
			values.put("card_id", card);
			values.put("cat_id", category);
			
			if(database.update("payment_details", values, "pid = "+pid, null)!=-1)
				return true;
		}
		return false;
	}
	
	//***************************************Card Summary******************************************************	

	protected SparseArray<ArrayList<String>> getCardSummary(int year, int mon, int date)
	{
		SparseArray<ArrayList<String>> cardTotals = new SparseArray<ArrayList<String>>();
		
		//Set values for this month, next month, last month and current date and year
		String thismonth=null;
		mon+=1;
		int yearprev=year;
		if(mon==1)
			yearprev=yearprev-1;
		int yearnext=year;
		if (mon==12)
			yearnext=yearnext+1;
		if(mon<10)
			thismonth="0"+mon;
		else
			thismonth = Integer.valueOf(mon).toString();
		String lastmonth=null;
		if(mon>1){
			if(mon<10)
				lastmonth="0"+(mon-1);
			else
				lastmonth=Integer.valueOf((mon-1)).toString();
		}
		else
			lastmonth="12"; //i.e. December
		String nextmonth=null;
		if(mon<12){
			if(mon<10){
				nextmonth="0"+(mon+1);
			} else {
				nextmonth=Integer.valueOf(mon+1).toString();
			}
		}
		else {
			nextmonth="01";
		}
		//find billing dates of all the cards 
		Cursor cardcursor = database.query
				("card_details",						//table 
				BillingDateColumns,						//columns
				null,							 		//selection
				null, 									//selection Args	
				null,									//groupby
				null, 									//having 			
				null);									//orderby
		cardcursor.moveToFirst();
		
		//fetch data for current bill cycle
		for (int i=0;i<cardcursor.getCount();i++)
		{
			String card_name = cardcursor.getString(1);
			String card_type = cardcursor.getString(2);
			String billing_date = cardcursor.getString(3);
			int billingdt = Integer.valueOf(billing_date);
			if (billingdt>=date)
			{
				Cursor cursor = database.query
						("payment_details",						//table 
						CardSummaryColumns,						//columns
						"card_id = '"+card_name+":"+card_type+"' AND "+
						"date > '"+yearprev+"/"+lastmonth+"/"+billing_date+"' AND "+
						"date <= '"+year+"/"+thismonth+"/"+billing_date+"'",
																//selection
						null, 									//selection Args	
						null,									//groupby
						null, 									//having 			
						null);									//orderby
				cursor.moveToFirst();
				ArrayList<String> values = null;
				values = new ArrayList<String>();
				values.add(card_name);
				values.add(card_type);
				if (cursor.getCount()>0)
				{
					String temp = Double.toString(cursor.getInt(1));
					values.add(temp);
				}
				else
					values.add("0");
				cardTotals.put(i,values);
				cursor.close();
			}
			else if (billingdt<date)
			{
				Cursor cursor = database.query
						("payment_details",						//table 
						CardSummaryColumns,						//columns
						"card_id = '"+card_name+":"+card_type+"' AND "+
						"date > '"+year+"/"+thismonth+"/"+billing_date+"' AND "+
						"date <= '"+yearnext+"/"+nextmonth+"/"+billing_date+"'", 			
																//selection
						null, 									//selection Args	
						null,							//groupby
						null, 									//having 			
						null);									//orderby
				cursor.moveToFirst();
				ArrayList<String> values = null;
				values = new ArrayList<String>();
				values.add(card_name);
				values.add(card_type);
				if (cursor.getCount()>0)
				{	
					String temp = Double.toString(cursor.getInt(1));
					values.add(temp);
				}
				else
					values.add("0");
				cardTotals.put(i,values);
				cursor.close();
			}
			cardcursor.moveToNext();
		}	
		cardcursor.close();
		return cardTotals;
	}

	
	//***************************************Cat Summary******************************************************	

	
	
		protected SparseArray<ArrayList<String>> getCatSummary(Integer month, int year)
		{
			SparseArray<ArrayList<String>> categoryTotals = new SparseArray<ArrayList<String>>();
			String Month = null;
			if(month<10){
				Month = "0"+month;
			} else {
				Month = Integer.valueOf(month).toString();
			}
			
			Cursor cursor = database.query
					("payment_details",						//table 
					CatSummaryColumns,						//columns
					"date LIKE '%"+year+"/"+Month+"%'", 		//selection
					null, 									//selection Args	
					"cat_id",						//groupby
					null, 									//having 			
					null);									//orderby
			cursor.moveToFirst();
			ArrayList<String> values = null;
			Map<String, String> tempCategories = new HashMap<String, String>();
					tempCategories.putAll(CardTrackActivity.catIdMap);
			int i;
			for(i=0; i<cursor.getCount();i++)
			{
				tempCategories.remove(cursor.getString(0));
				values = new ArrayList<String>();
				values.add(cursor.getString(0));
				values.add(cursor.getString(1));
				categoryTotals.put(i,values);
				cursor.moveToNext();
			}
				for (String category : tempCategories.keySet()) {
					values = new ArrayList<String>();
					values.add(category);
					values.add("0");
					categoryTotals.put(i++,values);
				}
			
			cursor.close();
			return categoryTotals;
		}

		public boolean settleCardAmount(String card_name, String card_type, String balance) {
			ContentValues values = new ContentValues();
			values.put("balance",balance);
			
			if(-1 != database.update("card_details", values, "card_name = '"+card_name+"' AND card_type='"+card_type+"'", null)){
					return true;
			}
			return false;
		}
}
