package com.wanderoo.cardtrack;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CardSummaryArrayAdaptor extends ArrayAdapter<String> 
{
	private final Context context;
	protected static CardSummaryActivity cardSummaryActivity = null;
	
	public CardSummaryArrayAdaptor(Context context, ArrayList<String> resources) 
	{
		super(context, R.layout.card_total, resources);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.card_total, parent, false);
		TextView cardName = (TextView) rowView.findViewById(R.id.cardName);
		TextView cardTotal = (TextView) rowView.findViewById(R.id.cardTotal);
		TextView cardBalance = (TextView) rowView.findViewById(R.id.cardBalance);
		
		cardName.setText(CardSummaryActivity.cardTotals.get(position).get(0)+":"+CardSummaryActivity.cardTotals.get(position).get(1));
		cardTotal.setText(CardSummaryActivity.cardTotals.get(position).get(2));
		cardBalance.setText(CardTrackActivity.cardIdMap.get(CardSummaryActivity.cardTotals.get(position).get(0)+":"+CardSummaryActivity.cardTotals.get(position).get(1)));
		
		Typeface face = Typeface.createFromAsset(cardSummaryActivity.getAssets(), "fonts/simplicity.ttf");
		cardName.setTypeface(face, Typeface.BOLD);
		cardTotal.setTypeface(face, Typeface.BOLD);
		face = Typeface.createFromAsset(cardSummaryActivity.getAssets(), "fonts/Harabara.ttf");
		cardBalance.setTypeface(face, Typeface.BOLD);
		
		return rowView;
	}
}