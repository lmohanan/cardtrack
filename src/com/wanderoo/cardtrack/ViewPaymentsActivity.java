package com.wanderoo.cardtrack;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class ViewPaymentsActivity extends Activity{
	protected static ArrayList<String> resources = new ArrayList<String>();
	protected static ViewPaymentsArrayAdapter listOfPayments = null;
	protected static ViewPaymentsActivity thisActivity = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_payments_list);
		thisActivity = this;
		
		ViewPaymentsArrayAdapter.viewPaymentsActivity=this;
		
		Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ALBA.TTF");
		TextView viewPaymentTitle = (TextView)findViewById(R.id.paymentDescTitle);
		viewPaymentTitle.setTypeface(face);
		
		viewPaymentTitle = (TextView)findViewById(R.id.paymentValueTitle);
		viewPaymentTitle.setTypeface(face);
		
		
		if(CardTrackActivity.payments.size()!=resources.size()){
			resources.clear();
			for(int i=0;i<CardTrackActivity.payments.size();i++){
				resources.add(Integer.valueOf(i).toString());
			}
		}
		
		listOfPayments = new ViewPaymentsArrayAdapter(this, resources);
		ListView paymentsList = (ListView) findViewById(R.id.paymentsList);
		paymentsList.setAdapter(listOfPayments);
		paymentsList.setOnItemClickListener(displayPaymentInfo);
	}
	
	private OnItemClickListener displayPaymentInfo = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
			ViewEditPaymentsActivity.rowSelected=pos;
			startActivity(new Intent(ViewPaymentsActivity.this, ViewEditPaymentsActivity.class));
		}
	};

	public void onBackPressed() {
		super.onBackPressed();
		ViewPaymentsActivity.this.finish();
	};
	
	public void loadPayments() 
	{
		if(null!=listOfPayments)
		listOfPayments.notifyDataSetChanged();
	}
}
