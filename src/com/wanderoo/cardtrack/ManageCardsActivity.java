package com.wanderoo.cardtrack;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ManageCardsActivity extends Activity {
	MySimpleArrayAdapter listOfCards = null;
	ArrayList<String> resources = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.card_list);
		MySimpleArrayAdapter.manageCardsActivity=this;
		for(int i=0; i<CardTrackActivity.cards.size();i++){
			resources.add(Integer.toString(i));
		}
		
		AddCardActivity.manageCardsActivity=this;
		ViewEditCardActivity.manageCardsActivity=this;
		
		listOfCards = new MySimpleArrayAdapter(this, resources);
		ListView cardListView = (ListView) findViewById(R.id.cardList);
		cardListView.setAdapter(listOfCards);
		cardListView.setOnItemClickListener(displayCardInfo);
	}

	public void loadCards() {
		resources.add(Integer.valueOf((CardTrackActivity.cards.size()-1)).toString());
		listOfCards.notifyDataSetChanged();
	}
	
	public void updateCardResource(){
		listOfCards.notifyDataSetChanged();
	}
	
	public void deleteCard(int rowSelected){
		resources.remove(rowSelected);
		listOfCards.notifyDataSetChanged();
	}

	public void onResume() {
		super.onResume();
	}

	private OnItemClickListener displayCardInfo = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
			ViewEditCardActivity.rowSelected=pos;
			startActivity(new Intent(ManageCardsActivity.this, ViewEditCardActivity.class));
		}
	};

	public void onBackPressed() {
		super.onBackPressed();
		ManageCardsActivity.this.finish();
	};

	public void addCard(View view) {
		startActivity(new Intent(this, AddCardActivity.class));
	}

}