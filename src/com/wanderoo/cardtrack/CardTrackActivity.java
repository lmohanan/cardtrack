package com.wanderoo.cardtrack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;


public class CardTrackActivity extends Activity{
	protected static CardTrackDataSource dataSource = null;
	protected static SparseArray<ArrayList<String>> cards = new SparseArray<ArrayList<String>>(10);
	protected static SparseArray<ArrayList<String>> categories = new SparseArray<ArrayList<String>>(10);
	protected static SparseArray<ArrayList<String>> payments = new SparseArray<ArrayList<String>>();
	
	protected static Map<String, String> cardIdMap = new HashMap<String, String>();
	protected static Map<String, String> catIdMap = new HashMap<String, String>();

	AdView adView = null;
	protected static List<String> monthList = new ArrayList<String>();
	protected static List<String> yearList = new ArrayList<String>();
	protected static List<String> cardTypes = new ArrayList<String>();
	protected static List<String> days = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		dataSource = new CardTrackDataSource(this);
		loadCards();
		loadCategories();
		loadPayments();
		
		monthList.add("Jan");
		monthList.add("Feb");
		monthList.add("Mar");
		monthList.add("Apr");
		monthList.add("May");
		monthList.add("Jun");
		monthList.add("Jul");
		monthList.add("Aug");
		monthList.add("Sep");
		monthList.add("Oct");
		monthList.add("Nov");
		monthList.add("Dec");

		for(int i=2000;i<2099;i++){
			yearList.add(Integer.valueOf(i).toString());
		}
		
		cardTypes.add("Credit");
		cardTypes.add("Debit");
		
		for(int i=1;i<32;i++){
			days.add(Integer.valueOf(i).toString());
		}
		
		TextView title = (TextView) findViewById(R.id.title);
		Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ALBA.TTF");
		title.setTypeface(face);
		
		face=Typeface.createFromAsset(getAssets(), "fonts/simplicity.ttf");
		
		Button button = (Button) findViewById(R.id.manageCards);
		button.setTypeface(face, Typeface.BOLD);
				
		button = (Button) findViewById(R.id.manageCategory);
		button.setTypeface(face, Typeface.BOLD);
		
		button = (Button) findViewById(R.id.addpayment);
		button.setTypeface(face, Typeface.BOLD);
		
		button = (Button) findViewById(R.id.cardssummary);
		button.setTypeface(face, Typeface.BOLD);
		
		button = (Button) findViewById(R.id.categorysummary);
		button.setTypeface(face, Typeface.BOLD);
		
		button = (Button) findViewById(R.id.viewpayments);
		button.setTypeface(face, Typeface.BOLD);

		try {

			adView = new AdView(this, AdSize.BANNER, "a15105ae41e6557");
			// Lookup your LinearLayout assuming it�s been given
			// the attribute android:id="@+id/adLayout"
			LinearLayout layout = (LinearLayout) findViewById(R.id.adLayout);

			// Add the adView to it
			layout.addView(adView);
			adView.loadAd(new AdRequest());
		} catch (Exception e) {
		}

	}
	
	public void manageCards(View view) {
		startActivity(new Intent(this, ManageCardsActivity.class));
	}
	
	public void addCategory(View view) {
		startActivity(new Intent(this, ManageCategoryActivity.class));
	}
	
	public void addPayment(View view) {
		if(CardTrackActivity.cards.size()<=0){
			showDialog(9);
		} else if(CardTrackActivity.categories.size()<=0){
			showDialog(10);
		} else {
			startActivity(new Intent(this, AddPaymentActivity.class));
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog dialog = null;
		Builder builder1 = null;
		switch (id) {
		case 9:
			// Create out AlterDialog
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("Please add a card before adding a payment...");
			builder1.setCancelable(true);
			builder1.setNeutralButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		case 10:
			// Create out AlterDialog
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("Please add a category before adding a payment...");
			builder1.setCancelable(true);
			builder1.setNeutralButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		}
		return dialog;
	}
	
	public void viewPayments(View view) {
		startActivity(new Intent(this, ViewPaymentsActivity.class));
	}
	
	public void cardsSummary(View view) {
		startActivity(new Intent(this, CardSummaryActivity.class));
	}
	
	public void categoriesSummary(View view) {
		startActivity(new Intent(this, CatSummaryActivity.class));
	}
	
	public static void loadCards(){
		dataSource.open();
		dataSource.getCards(cards);
		dataSource.close();
		
		for(int i=0;i<cards.size();i++){
			cardIdMap.put(cards.get(i).get(1)+":"+cards.get(i).get(3), cards.get(i).get(5));
		}
	}
	
	public static void loadCategories()
	{
		dataSource.open();
		dataSource.getCategories(categories);
		dataSource.close();
		
		for(int i=0;i<categories.size();i++){
			catIdMap.put(categories.get(i).get(1), categories.get(i).get(0));
		}
	}
	
	public static void loadPayments(){
		dataSource.open();
		dataSource.getPayments(payments);
		dataSource.close();
	}
	
	public void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
		android.os.Process.killProcess(android.os.Process.myPid());
	}

}
