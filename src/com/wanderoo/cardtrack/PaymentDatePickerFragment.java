package com.wanderoo.cardtrack;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

public class PaymentDatePickerFragment extends DialogFragment implements
		DatePickerDialog.OnDateSetListener {
	protected static boolean editing = false;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		if(editing){
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy/mm/dd", Locale.US);
				c.setTime(formatter.parse(ViewEditPaymentsActivity.thisActivity.payDate));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return new DatePickerDialog(ViewEditPaymentsActivity.thisActivity, this,
					c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DAY_OF_MONTH));
		}
		else
			return new DatePickerDialog(AddPaymentActivity.thisActivity, this,
				year, month, day);
	}

	public void onDateSet(DatePicker view, int year, int month, int day) {
		month+=1;
		String dayOfTheMonth = Integer.valueOf(day).toString();
		if(day<10){
			dayOfTheMonth = "0"+dayOfTheMonth;
		}
		String Month = Integer.valueOf(month).toString();
		if(month<10){
			Month = "0"+month;
		}
		if(editing){
			ViewEditPaymentsActivity.thisActivity.paymentDate.setText("Payment Date\n"
					+ year + "/"
					+ Month + "/" + dayOfTheMonth);
			ViewEditPaymentsActivity.thisActivity.payDate = + year + "/"
					+ Month + "/" + dayOfTheMonth;
		} else {
			AddPaymentActivity.thisActivity.paymentDate.setText("Payment Date\n"
					+ year + "/"
					+ Month + "/" + dayOfTheMonth);
			AddPaymentActivity.thisActivity.payDate = + year + "/"
					+ Month + "/" + dayOfTheMonth;
		}
	}
}