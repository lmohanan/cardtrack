package com.wanderoo.cardtrack;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ViewPaymentsArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	protected static ViewPaymentsActivity viewPaymentsActivity = null;

	public ViewPaymentsArrayAdapter(Context context, ArrayList<String> resources) {
		super(context, R.layout.payments_info, resources);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.payments_info, parent, false);
		TextView paymentId = (TextView) rowView.findViewById(R.id.paymentId);
		
		paymentId.setText(CardTrackActivity.payments.get(position).get(0));
		
		Typeface face = Typeface.createFromAsset(viewPaymentsActivity.getAssets(), "fonts/COMIC.TTF");
		
		TextView paymentDesc = (TextView) rowView.findViewById(R.id.PaymentDesc);
		paymentDesc.setTypeface(face, Typeface.BOLD);
		if(CardTrackActivity.payments.get(position).get(2).contains(CardTrackActivity.payments.get(position).get(1))){
			paymentDesc.setText(CardTrackActivity.payments.get(position).get(2));
		} else {
			paymentDesc.setText(CardTrackActivity.payments.get(position).get(2)+"("+CardTrackActivity.payments.get(position).get(1)+")");
		}
		
		TextView value = (TextView) rowView.findViewById(R.id.PaymentValue);
		value.setTypeface(face);
		value.setText(CardTrackActivity.payments.get(position).get(3));
		
		return rowView;
	}
}