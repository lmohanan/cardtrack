package com.wanderoo.cardtrack;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ManageCategoryActivity extends Activity 
{
	CategoryArrayAdapter listOfCategory = null;
	ArrayList<String> resources = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.category_list);
		CategoryArrayAdapter.manageCategoryActivity=this;
		ViewEditCategoryActivity.manageCategorysActivity=this;
		AddCategoryActivity.manageCategoryActivity=this;
		
		for(int i=0; i<CardTrackActivity.categories.size();i++)
		{
			resources.add(Integer.toString(i));
		}
		
		listOfCategory = new CategoryArrayAdapter(this, resources);
		ListView categoryListView = (ListView) findViewById(R.id.categoryList);
		categoryListView.setAdapter(listOfCategory);
		categoryListView.setOnItemClickListener(displayCategoryInfo);
	}

	public void loadCategories() 
	{
		//resources.add(Integer.valueOf((CardTrackActivity.categories.size()-1)).toString());
		listOfCategory.notifyDataSetChanged();
	}

	
	public void onResume() 
	{
		super.onResume();
	}

	private OnItemClickListener displayCategoryInfo = new OnItemClickListener() 
	{
		public void onItemClick(AdapterView<?> av, View v, int pos, long id) 
		{
			ViewEditCategoryActivity.rowSelected=pos;
			startActivity(new Intent(ManageCategoryActivity.this, ViewEditCategoryActivity.class));
		}
	};

	public void onBackPressed() 
	{
		super.onBackPressed();
		ManageCategoryActivity.this.finish();
	};

	public void addCategory(View view) 
	{
		startActivity(new Intent(this, AddCategoryActivity.class));
	}

	public void addCategory() {
		resources.add(Integer.valueOf((CardTrackActivity.categories.size()-1)).toString());
		listOfCategory.notifyDataSetChanged();
	}

}