package com.wanderoo.cardtrack;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ViewEditCardActivity extends Activity{
	protected static int rowSelected;
	String oldCardName = null;
	EditText cardName = null;
	EditText cardLimit = null;
	Double oldCardLimit = null;
	Spinner cardType = null;
	protected Spinner cardBillingDate = null;
	protected static ManageCardsActivity manageCardsActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.view_edit_card_layout);
		
		Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ALBA.TTF");
		TextView viewCardTitle = (TextView)findViewById(R.id.cardDetails);
		viewCardTitle.setTypeface(face);
		
		cardName = (EditText) findViewById(R.id.cardName);
		cardLimit = (EditText) findViewById(R.id.cardLimit);
		
		face = Typeface.createFromAsset(getAssets(), "fonts/Harabara.ttf");
		cardName.setTypeface(face);
		cardLimit.setTypeface(face);
		
		cardName.setText(CardTrackActivity.cards.get(rowSelected).get(1));
		oldCardName = CardTrackActivity.cards.get(rowSelected).get(1);
		
		cardLimit.setText(CardTrackActivity.cards.get(rowSelected).get(2));
		oldCardLimit = Double.valueOf(CardTrackActivity.cards.get(rowSelected).get(2));
		
		cardType = (Spinner) findViewById(R.id.cardType);

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, CardTrackActivity.cardTypes);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cardType.setAdapter(dataAdapter);
		
		if("credit".equalsIgnoreCase(CardTrackActivity.cards.get(rowSelected).get(3)))
			cardType.setSelection(0);
		else
			cardType.setSelection(1);
		cardBillingDate = (Spinner) findViewById(R.id.cardBillingDate);
		
		dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, CardTrackActivity.days);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cardBillingDate.setAdapter(dataAdapter);
		
		cardBillingDate.setSelection(Integer.valueOf(CardTrackActivity.cards.get(rowSelected).get(4))-1);
	}

	public void editCard(View view){
		showDialog(12);
	}
	
	public void editTheCard(){
		String cardName = this.cardName.getText().toString();
		Double cardLimit = Double.valueOf(this.cardLimit.getText().toString());
		String cardType = this.cardType.getSelectedItem().toString();
		
		CardTrackActivity.dataSource.open();
		Double balance = Double.valueOf(CardTrackActivity.cards.get(rowSelected).get(5)) + (cardLimit-oldCardLimit);
		if(CardTrackActivity.dataSource.updateCardDetails(oldCardName,Integer.valueOf(CardTrackActivity.cards.get(rowSelected).get(0)), cardName, cardLimit.toString(), cardType,
				cardBillingDate.getSelectedItem().toString(), balance.toString())){
			CardTrackActivity.cards.get(rowSelected).set(1, cardName);
			CardTrackActivity.cards.get(rowSelected).set(2, cardLimit.toString());
			CardTrackActivity.cards.get(rowSelected).set(3, cardType);
			CardTrackActivity.cards.get(rowSelected).set(4, cardBillingDate.getSelectedItem().toString());
			CardTrackActivity.cards.get(rowSelected).set(5, balance.toString());
			Toast.makeText(ViewEditCardActivity.this, "Card details updated successfully...", Toast.LENGTH_LONG).show();
			manageCardsActivity.updateCardResource();
			
			for(int i=0;i<CardTrackActivity.payments.size();i++){
				if(CardTrackActivity.payments.get(i).get(4).equals(oldCardName))
					CardTrackActivity.payments.get(i).set(4, cardName);
			}
			
		} else {
			Toast.makeText(ViewEditCardActivity.this, "Some error occurred...", Toast.LENGTH_LONG).show();
		}
		CardTrackActivity.dataSource.close();
		ViewEditCardActivity.this.finish();
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog dialog = null;
		Builder builder1 = null;
		switch (id) {
		case 11:
			// Create out AlterDialog
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("Are you sure you want to delete this card?\nDeleting the card will delete all the payments associated with the card...");
			builder1.setCancelable(true);
			builder1.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Toast.makeText(getApplicationContext(),
									"Card deletion cancelled...", Toast.LENGTH_SHORT).show();
						}
					});
			builder1.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							deleteTheCard();
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		case 12:
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("Are you sure you want to update the card?");
			builder1.setCancelable(true);
			builder1.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Toast.makeText(getApplicationContext(),
									"Card updation cancelled...", Toast.LENGTH_SHORT).show();
						}
					});
			builder1.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							editTheCard();
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		}
		return dialog;
	}
	
	public void deleteCard(View view){
		showDialog(11);
	}
	
	public void deleteTheCard(){
		String cardName = this.cardName.getText().toString();
		String cardType = this.cardType.getSelectedItem().toString();
		
		CardTrackActivity.dataSource.open();
		if(CardTrackActivity.dataSource.deleteCard(cardName, cardType)){
			Toast.makeText(ViewEditCardActivity.this, "Card deleted successfully...", Toast.LENGTH_LONG).show();
			CardTrackActivity.cards.clear();
			CardTrackActivity.loadCards();
			if(null!=CardTrackActivity.payments)
			for(int i=0;i<CardTrackActivity.payments.size();i++){
				if(CardTrackActivity.payments.get(i).get(4).equals(cardName)){
					CardTrackActivity.payments.delete(i);
				}
			}
			CardTrackActivity.dataSource.close();
			manageCardsActivity.deleteCard(rowSelected);
			this.finish();
		} else {
			Toast.makeText(ViewEditCardActivity.this, "Some error occurred...", Toast.LENGTH_LONG).show();
		}
		CardTrackActivity.dataSource.close();
	}
	
	public void cancel(View view){
		this.finish();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
	}
}
