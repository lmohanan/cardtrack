package com.wanderoo.cardtrack;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.SparseArray;

public class DataManager extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "cardtrack.db";
	private static final int DATABASE_VERSION = 1;
	
	private SparseArray<ArrayList<String>> cardsArray = new SparseArray<ArrayList<String>>(10);
	private SparseArray<ArrayList<String>> categoryArray = new SparseArray<ArrayList<String>>(10);
	private SparseArray<ArrayList<String>> paymentsArray = new SparseArray<ArrayList<String>>(25);

	public DataManager(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL("CREATE TABLE card_details("+
				"id INTEGER PRIMARY KEY,"+
				"card_name TEXT NOT NULL,"+
				"card_limit TEXT NOT NULL,"+
				"card_type TEXT NOT NULL," +
				"billing_date TEXT NOT NULL," +
				"balance TEXT NOT NULL);"
				);
		
		ContentValues values = new ContentValues();
		ArrayList<String> vals = null;
		for(int i=0; i<cardsArray.size();i++){
			vals = cardsArray.get(i);
			values.put("card_name",vals.get(0));
			values.put("card_limit", vals.get(1));
			values.put("card_type", vals.get(2));
			values.put("billing_date", vals.get(3));
			database.insert("card_details", null, values);
			vals.clear();
		}
		
	//Table for categories	
		database.execSQL("CREATE TABLE category_details("+
				"cid INTEGER PRIMARY KEY,"+
				"category_name TEXT NOT NULL);"
				);

		ContentValues cat_values = new ContentValues();
		if(categoryArray.size()>0){
			for(int i=0;i<categoryArray.size();i++){
				vals.clear();
				vals = categoryArray.get(i);
				cat_values.put("category_name",vals.get(0));
				database.insert("category_details", null, cat_values);
			}
		} else {
			cat_values.put("category_name", "Education");
			database.insert("category_details", null, cat_values);
			cat_values.put("category_name", "Groceries");
			database.insert("category_details", null, cat_values);
			cat_values.put("category_name", "Travel");
			database.insert("category_details", null, cat_values);
			cat_values.put("category_name", "Rent");
			database.insert("category_details", null, cat_values);
			cat_values.put("category_name", "General Merchandise");
			database.insert("category_details", null, cat_values);
			cat_values.put("category_name", "Restaurants/Dining");
			database.insert("category_details", null, cat_values);
			cat_values.put("category_name", "Clothing");
			database.insert("category_details", null, cat_values);
			cat_values.put("category_name", "Home Improvement");
			database.insert("category_details", null, cat_values);
			cat_values.put("category_name", "Gasoline");
			database.insert("category_details", null, cat_values);
		}
		
		
	//Table for payment	
		database.execSQL("CREATE TABLE payment_details("+
				"pid INTEGER PRIMARY KEY,"+
				"date TEXT,"+
				"desc TEXT,"+
				"amt INTEGER,"+
				"card_id TEXT,"+
				"cat_id TEXT);"
				);
		ContentValues payment_values = new ContentValues();
		for(int i=0;i<paymentsArray.size();i++){
			vals.clear();
			vals = paymentsArray.get(i);
			payment_values.put("date",vals.get(0));
			payment_values.put("desc",vals.get(1));
			payment_values.put("amt",vals.get(2));
			payment_values.put("card_id",vals.get(3));
			payment_values.put("cat_id",vals.get(4));
			database.insert("payment_details", null, payment_values);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Cursor cursor = db.rawQuery("SELECT COUNT(1) FROM sqlite_master WHERE type = ? AND name = ?", new String[] {"table", "card_details"});
		if(cursor.moveToFirst()){
			cursor.close();
			cardsArray.clear();
			cursor = db.query("card_details", CardTrackDataSource.cardColumns, null, null, null, null, null);
			cursor.moveToFirst();
			ArrayList<String> values = null;
			for(int i=0; i<cursor.getCount();i++){
				values = new ArrayList<String>();
				values.add(cursor.getString(1));
				values.add(cursor.getString(2));
				values.add(cursor.getString(3));
				values.add(cursor.getString(4));
				values.add(cursor.getString(5));
				cardsArray.put(i, values);
				cursor.moveToNext();
			}
			cursor.close();
		}
		
		cursor = db.rawQuery("SELECT COUNT(1) FROM sqlite_master WHERE type = ? AND name = ?", new String[] {"table", "category_details"});
		if(cursor.moveToFirst()){
			cursor.close();
			categoryArray.clear();
			cursor = db.query("category_details", CardTrackDataSource.CategoryColumns, null, null, null, null, null);
			cursor.moveToFirst();
			ArrayList<String> values = null;
			for(int i=0; i<cursor.getCount();i++)
			{
				values = new ArrayList<String>();
				values.add(cursor.getString(1));
				categoryArray.put(i, values);
				cursor.moveToNext();
			}
			cursor.close();
		}
		
		cursor = db.rawQuery("SELECT COUNT(1) FROM sqlite_master WHERE type = ? AND name = ?", new String[] {"table", "payment_details"});
		if(cursor.moveToFirst()){
			cursor.close();
			paymentsArray.clear();
			cursor = db.query("payment_details", CardTrackDataSource.paymentColumns, null, null, null, null, null);
			cursor.moveToFirst();
			ArrayList<String> values = null;
			for(int i=0; i<cursor.getCount();i++)
			{
				values = new ArrayList<String>();
				values.add(cursor.getString(1));
				values.add(cursor.getString(2));
				values.add(cursor.getString(3));
				values.add(cursor.getString(4));
				values.add(cursor.getString(5));
				paymentsArray.put(i, values);
				cursor.moveToNext();
			}
			cursor.close();
		}
		
		db.execSQL("DROP TABLE IF EXISTS card_details;");
		db.execSQL("DROP TABLE IF EXISTS category_details;");
		db.execSQL("DROP TABLE IF EXISTS payment_details;");
		onCreate(db);
	}
}
