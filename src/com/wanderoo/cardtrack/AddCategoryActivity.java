package com.wanderoo.cardtrack;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddCategoryActivity extends Activity 
{
	protected EditText CategoryName = null;
	protected static ManageCategoryActivity manageCategoryActivity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.add_category_layout);
		
		TextView addCategoryTitle = (TextView) findViewById(R.id.addCategoryTitle);
		Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ALBA.TTF");
		addCategoryTitle.setTypeface(face);
		
		CategoryName = (EditText) findViewById(R.id.CategoryName);
		
		face = Typeface.createFromAsset(getAssets(), "fonts/Harabara.ttf");
		CategoryName.setTypeface(face);
		addCategoryTitle.setTypeface(face);
	}
	
	public void addCategory(View view)
	{
		if(this.CategoryName.getText().toString().equals("")){
			Toast.makeText(AddCategoryActivity.this, "Please enter category name...", Toast.LENGTH_LONG).show();
			return;
		}
		
		String CategoryName = this.CategoryName.getText().toString();
		
		CardTrackActivity.dataSource.open();
		if(CardTrackActivity.dataSource.checkIfCategoryExists(CategoryName))
		{
			Toast.makeText(AddCategoryActivity.this, "Category already exists.\nPlease enter a different Category.", Toast.LENGTH_LONG).show();
			return;
		}
		
		if(CardTrackActivity.dataSource.insertCategoryDetails(CategoryName))
		{
			Toast.makeText(AddCategoryActivity.this, "Category added successfully...", Toast.LENGTH_LONG).show();
			CardTrackActivity.categories.clear();
			CardTrackActivity.loadCategories();
			CardTrackActivity.dataSource.close();
			manageCategoryActivity.addCategory();
			this.finish();
		} 
		else 
		{
			Toast.makeText(AddCategoryActivity.this, "Error! Category Not Added...", Toast.LENGTH_LONG).show();
		}
		CardTrackActivity.dataSource.close();
	}
	
	public void cancel(View view)
	{
		this.finish();
	}
	
	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();
		this.finish();
	}
}
