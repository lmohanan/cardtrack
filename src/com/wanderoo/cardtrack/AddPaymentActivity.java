package com.wanderoo.cardtrack;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddPaymentActivity extends FragmentActivity {
	protected Spinner paymentCard = null;
	protected Spinner paymentCategory = null;
	protected EditText paymentDesc = null;
	protected EditText paymentAmt = null;
	protected static AddPaymentActivity thisActivity = null;
	protected Button paymentDate = null;
	protected String payDate = null;
	private int year;
	private String day;
	private String month;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.add_payment_layout);
		
		thisActivity = this;
		PaymentDatePickerFragment.editing = false;

		TextView addPaymentDesc = (TextView) findViewById(R.id.addPaymentdesc);
		Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ALBA.TTF");
		addPaymentDesc.setTypeface(face);

		paymentCard = (Spinner) findViewById(R.id.PaymentCard);

		List<String> list = new ArrayList<String>();
		if (CardTrackActivity.cards.size() > 0)
			for (int i = 0; i < CardTrackActivity.cards.size(); i++)
				list.add(CardTrackActivity.cards.get(i).get(1).toString()+":"+CardTrackActivity.cards.get(i).get(3).toString());

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		paymentCard.setAdapter(dataAdapter);

		paymentCategory = (Spinner) findViewById(R.id.PaymentCategory);
		list = new ArrayList<String>();
		if (CardTrackActivity.categories.size() > 0)
			for (int i = 0; i < CardTrackActivity.categories.size(); i++)
				list.add(CardTrackActivity.categories.get(i).get(1));

		dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		paymentCategory.setAdapter(dataAdapter);

		paymentDesc = (EditText) findViewById(R.id.PaymentDesc);
		paymentAmt = (EditText) findViewById(R.id.PaymentAmt);

		face = Typeface.createFromAsset(getAssets(), "fonts/Harabara.ttf");
		paymentDesc.setTypeface(face);
		paymentAmt.setTypeface(face);

		paymentDate = (Button) findViewById(R.id.paymentDate);
		final Calendar c = Calendar.getInstance();
		String dayOfTheMonth = Integer.valueOf(c.get(Calendar.DAY_OF_MONTH)).toString();
		if(c.get(Calendar.DAY_OF_MONTH)<10){
			dayOfTheMonth = "0"+dayOfTheMonth;
		}
		String month = Integer.valueOf(c.get(Calendar.MONTH)+1).toString();
		if(c.get(Calendar.MONTH)<10){
			month = "0"+month;
		}
		paymentDate.setText("Payment Date\n"
				+ c.get(Calendar.YEAR) + "/"
				+ month + "/" + dayOfTheMonth);
		paymentDate.setTypeface(face);
		payDate = + c.get(Calendar.YEAR) + "/"
				+ month + "/" + dayOfTheMonth;
	}

	public void addPayment(View view) {
		int flag = 1;
		Double paymentAmt = null;
		String paymentCard = "";
		if (null != this.paymentCard.getSelectedItem()) {
			paymentCard = this.paymentCard.getSelectedItem().toString();
		} else {
			flag = 0;
		}
		// Validate Category
		String paymentCategory = "";
		if (null != this.paymentCategory.getSelectedItem()) {
			paymentCategory = this.paymentCategory.getSelectedItem().toString();
		} else {
			flag = 0;
		}

		// Vaidate and set defaults for amt
		if (this.paymentAmt.getText().length() <= 0) {
			Toast.makeText(this, "Please enter an amount...", Toast.LENGTH_LONG).show();
			return;
		} else {
			try{
				paymentAmt = Double.parseDouble(this.paymentAmt.getText().toString());
			} catch(NumberFormatException ne){
				Toast.makeText(this, "Please enter an amount greater than zero...", Toast.LENGTH_LONG).show();
				return;
			}
		}

		// validate and set defaults for description
		String paymentDescription = null;
		if (this.paymentDesc.getText().toString().equals("")
				|| this.paymentDesc.getText().toString().equals(
						"Enter Payment Description"))
			paymentDescription = paymentCategory + " (" + payDate + ")";
		else
			paymentDescription = this.paymentDesc.getText().toString();

		CardTrackActivity.dataSource.open();
		
		String balance = CardSummaryActivity.df.format(Double.valueOf(CardTrackActivity.cardIdMap.get(paymentCard))-paymentAmt);

		if (flag == 1) {
			if (CardTrackActivity.dataSource.insertPaymentDetails(payDate, paymentDescription, paymentAmt.toString(), paymentCard, paymentCategory, balance)) {
				
				CardTrackActivity.cardIdMap.put(paymentCard, balance);
				ArrayList<String> values = new ArrayList<String>();
				int pid = CardTrackActivity.payments.size()>0 ? Integer.valueOf(CardTrackActivity.payments.get(CardTrackActivity.payments.size()-1).get(0)+1):1;
				values.add(Integer.valueOf(pid).toString());
				values.add(payDate);
				values.add(paymentDescription);
				values.add(paymentAmt.toString());
				values.add(paymentCard);
				values.add(paymentCategory);
				CardTrackActivity.payments.append(CardTrackActivity.payments.size(), values);
				
				Toast.makeText(AddPaymentActivity.this,
						"Payment added successfully...", Toast.LENGTH_LONG)
						.show();
				
				if(Double.valueOf(balance)<=0){
					showDialog(11);
				} else {
					Double limit=0.0;
					for(int i=0;i<CardTrackActivity.cards.size();i++){
						if(CardTrackActivity.cards.get(i).get(1).equals(paymentCard.split(":")[0]) &&
								CardTrackActivity.cards.get(i).get(3).equals(paymentCard.split(":")[1])){
							limit = Double.valueOf(CardTrackActivity.cards.get(i).get(2));
							break;
						}
					}
					if(Double.valueOf(balance)<=(0.1*limit)){
						showDialog(15);
					} else if(Double.valueOf(balance)<=(0.25*limit)){
						showDialog(14);
					} else if(Double.valueOf(balance)<=(0.5*limit)){
						showDialog(13);
					} else if (Double.valueOf(balance)<=(0.75*limit)){
						showDialog(12);
					} else {
						CardTrackActivity.dataSource.close();
						AddPaymentActivity.thisActivity.finish();
					}
				}
			} else {
				Toast.makeText(AddPaymentActivity.this,
						"Error! Payment Not Added...", Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(AddPaymentActivity.this,
					"Error! Payment Not Added...", Toast.LENGTH_LONG).show();
		}
	}

	public void cancel(View view) {
		this.finish();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
	}

	public void showDatePickerDialog(View view) {
		DialogFragment newFragment = new PaymentDatePickerFragment();
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog dialog = null;
		Builder builder1 = null;
		switch (id) {
		case 11:
			// Create out AlterDialog
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("ALERT: Your "+paymentCard.getSelectedItem().toString()+" card limit is over!");
			builder1.setCancelable(true);
			builder1.setNeutralButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							CardTrackActivity.dataSource.close();
							AddPaymentActivity.thisActivity.finish();
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		case 12:
			// Create out AlterDialog
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("ALERT: You have 75% limit of your "+paymentCard.getSelectedItem().toString()+" card remaining!");
			builder1.setCancelable(true);
			builder1.setNeutralButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							CardTrackActivity.dataSource.close();
							AddPaymentActivity.thisActivity.finish();
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		case 13:
			// Create out AlterDialog
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("ALERT: You have 50% limit of your "+paymentCard.getSelectedItem().toString()+" card remaining!");
			builder1.setCancelable(true);
			builder1.setNeutralButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							CardTrackActivity.dataSource.close();
							AddPaymentActivity.thisActivity.finish();
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		case 14:
			// Create out AlterDialog
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("ALERT: You have 25% limit of your "+paymentCard.getSelectedItem().toString()+" card remaining!");
			builder1.setCancelable(true);
			builder1.setNeutralButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							CardTrackActivity.dataSource.close();
							AddPaymentActivity.thisActivity.finish();
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		case 15:
			// Create out AlterDialog
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("WARNING: Only 10% limit of your "+paymentCard.getSelectedItem().toString()+" card remains!");
			builder1.setCancelable(true);
			builder1.setNeutralButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							CardTrackActivity.dataSource.close();
							AddPaymentActivity.thisActivity.finish();
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		case 16:
			// Create out AlterDialog
			builder1 = new AlertDialog.Builder(this);
			builder1.setMessage("ALERT: The date you selected is in the future. Are you sure you want to continue?");
			builder1.setCancelable(true);
			builder1.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			builder1.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							AddPaymentActivity.thisActivity.paymentDate.setText("Payment Date\n"
									+ year + "/"
									+ month + "/" + day);
							AddPaymentActivity.thisActivity.payDate = + year + "/"
									+ month + "/" + day;
						}
					});
			dialog = builder1.create();
			dialog.show();
			break;
		
		}
		return dialog;
	}
	
	public void continueWithDateSelection(int year, String month2, String dayOfTheMonth){
		this.year = year;
		this.month = month2;
		this.day = dayOfTheMonth;
		showDialog(16);
	}
}