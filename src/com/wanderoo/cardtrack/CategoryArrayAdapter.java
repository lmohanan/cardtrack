package com.wanderoo.cardtrack;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CategoryArrayAdapter extends ArrayAdapter<String> 
{
	private final Context context;
	protected static ManageCategoryActivity manageCategoryActivity = null;
	
	public CategoryArrayAdapter(Context context, ArrayList<String> resources) 
	{
		super(context, R.layout.category_info, resources);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.category_info, parent, false);
		TextView categoryName = (TextView) rowView.findViewById(R.id.categoryName);
		
		categoryName.setText(CardTrackActivity.categories.get(position).get(1));
		
		Typeface face = Typeface.createFromAsset(manageCategoryActivity.getAssets(), "fonts/COMIC.TTF");
		categoryName.setTypeface(face, Typeface.BOLD);
		
		return rowView;
	}
}