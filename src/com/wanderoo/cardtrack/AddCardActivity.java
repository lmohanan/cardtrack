package com.wanderoo.cardtrack;

import java.util.Calendar;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddCardActivity extends Activity {
	protected Spinner cardType = null;
	protected EditText cardName = null;
	protected EditText cardLimit = null;
	protected Spinner cardBillingDate = null;
	protected static ManageCardsActivity manageCardsActivity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.add_card_layout);
		
		TextView addCardTitle = (TextView) findViewById(R.id.addCardTitle);
		Typeface face = Typeface.createFromAsset(getAssets(), "fonts/ALBA.TTF");
		addCardTitle.setTypeface(face);
		
		cardType = (Spinner) findViewById(R.id.cardType);
		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, CardTrackActivity.cardTypes);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cardType.setAdapter(dataAdapter);
		
		cardBillingDate = (Spinner) findViewById(R.id.cardBillingDate);
		
		dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, CardTrackActivity.days);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cardBillingDate.setAdapter(dataAdapter);
		
		cardBillingDate.setSelection(Calendar.getInstance().get(Calendar.DAY_OF_MONTH)-1);
		
		cardName = (EditText) findViewById(R.id.cardName);
		cardLimit = (EditText) findViewById(R.id.cardLimit);
		
		face = Typeface.createFromAsset(getAssets(), "fonts/Harabara.ttf");
		cardName.setTypeface(face);
		cardLimit.setTypeface(face);
		
		TextView billingDate = (TextView) findViewById(R.id.billingDate);
		billingDate.setTypeface(face);
	}
	
	public void addCard(View view){
		if(this.cardName.getText().toString().equals("")){
			Toast.makeText(AddCardActivity.this, "Please enter card name...", Toast.LENGTH_LONG).show();
			return;
		}
		
		String cardName = this.cardName.getText().toString();
		Double cardLimit = 0.0;
		try{
			cardLimit = Double.parseDouble(this.cardLimit.getText().toString());
		} catch(NumberFormatException ne){
			Toast.makeText(AddCardActivity.this, "Please enter a valid card limit greater than zero...", Toast.LENGTH_LONG).show();
			return;
		}
		String cardType = this.cardType.getSelectedItem().toString();
		
		CardTrackActivity.dataSource.open();
		if(CardTrackActivity.dataSource.checkIfCardExists(cardName, cardType)){
			Toast.makeText(AddCardActivity.this, "Card already exists.\nPlease enter a different card name or type.", Toast.LENGTH_LONG).show();
			return;
		}
		
		if(CardTrackActivity.dataSource.insertCardDetails(cardName, cardLimit.toString(), cardType, cardBillingDate.getSelectedItem().toString(),cardLimit.toString())){
			Toast.makeText(AddCardActivity.this, "Card added successfully...", Toast.LENGTH_LONG).show();
			CardTrackActivity.cards.clear();
			CardTrackActivity.loadCards();
			CardTrackActivity.dataSource.close();
			manageCardsActivity.loadCards();
			this.finish();
		} else {
			Toast.makeText(AddCardActivity.this, "Some error occurred...", Toast.LENGTH_LONG).show();
		}
		CardTrackActivity.dataSource.close();
	}
	
	public void cancel(View view){
		this.finish();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.finish();
	}
}
