package com.wanderoo.cardtrack;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

public class CardSettlementActivity extends Activity
{
	protected static int rowSelected;
	private String card_name;
	private String card_type;
	protected EditText settlementAmount = null;
	protected EditText paymentCard = null;
	protected EditText paymentCardType = null;
	protected static CardSettlementActivity thisActivity = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		thisActivity = this;
		setContentView(R.layout.settle);
		
		settlementAmount = (EditText) findViewById(R.id.settlementAmount);
		
		Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Harabara.ttf");
		settlementAmount.setTypeface(face);
		
		paymentCard = (EditText) findViewById(R.id.PaymentCard);
		paymentCardType = (EditText) findViewById(R.id.PaymentCardType);
		
		paymentCard.setTypeface(face);
		paymentCardType.setTypeface(face);
		
		card_name = CardSummaryActivity.cardTotals.get(rowSelected).get(0);
		card_type = CardSummaryActivity.cardTotals.get(rowSelected).get(1);
		
		paymentCard.setText("  "+CardSummaryActivity.cardTotals.get(rowSelected).get(0)+"  ");
		paymentCardType.setText("  "+CardSummaryActivity.cardTotals.get(rowSelected).get(1)+"  ");
	}
	
	public void settleAmount(View view)
	{
		Double settleAmt = 0.0;
		try{
			settleAmt = Double.parseDouble(((EditText)findViewById(R.id.settlementAmount)).getText().toString());
		} catch(NumberFormatException ne){
			Toast.makeText(CardSettlementActivity.this, "Please enter a valid settlement amount greater than zero...", Toast.LENGTH_LONG).show();
			return;
		}
		String balance = CardSummaryActivity.df.format(settleAmt + Double.valueOf(CardTrackActivity.cardIdMap.get(card_name+":"+card_type)));
		CardTrackActivity.dataSource.open();
		
		if(CardTrackActivity.dataSource.settleCardAmount(card_name,card_type,balance))
		{
			CardTrackActivity.cardIdMap.put(card_name+":"+card_type, balance);
						
			Toast.makeText(CardSettlementActivity.this, "Amount settled successfully...", Toast.LENGTH_LONG).show();
			
			CardSummaryActivity.thisActivity.loadCards();
		} 
		else 
		{
			Toast.makeText(CardSettlementActivity.this, "Error! Category not updated...", Toast.LENGTH_LONG).show();
		}
		CardTrackActivity.dataSource.close();
		CardSettlementActivity.this.finish();
	}
		
	public void cancelOperation(View view)
	{
		this.finish();
	}
	
	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();
		this.finish();
	}
}
